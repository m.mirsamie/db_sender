var mysql = require('mysql');
var net = require('net');
var trim = require('trim');
var server = "localhost";
var port = 4050;
var client = new net.Socket();
var funcs = {};
var resp;
var resp1;
var sendCellFn;
var sendCellFn2;
var phone1;
var msg1;
var rid = 0;
var datas = [];
var data_index = 0;
var gfn;
var send_res = [];
var send_call;
var connected = false;
var send_timout = 3000;
var sending = false;
var send_timer;

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin_send',
    password: '159951',
    database: 'admin_send'
});
connection.connect();

connection.query('SELECT * from outBox where en = 0 limit 10', function (err, rows, fields) {
    if (err)
        throw err;
    datas = rows;
    startSending();
});

//connection.end();
client.on('data', function (data) {
    //console.log('send_timer = ',send_timer);
    //console.log('data = ',String(data));
    clearTimeout(send_timer);
    if(typeof gfn === 'function')
    {
        gfn(String(data));
    }
});
client.on('close', function () {
    connected = false;
});
client.on('error', function (e) {
    connected = false;
    console.log('error',e);
});
function parseResult(res)
{
    res = String(res);
    var out = res;
    if(res!=='TIMEOUT')
    {
        var tmp = res.split("\n");
        if(tmp.length>=2)
        {
            var obj = JSON.parse(tmp[1]);
            out = trim(obj.result);
        }
        else
        {
            out = 'UNKNOWN';
        }
    }
    return(out);
}
function setT()
{
    send_timer = setTimeout(function(){
        if(typeof gfn === 'function')
        {
            gfn('TIMEOUT');
        }
    },send_timout);
}
function sendCommand(cmd)
{
    //console.log(cmd);
    if (connected)
    {
        setT();
        client.write(cmd);
    }
    else
    {
        client.destroy();
        client.connect(port, server, function () {
            connected = true;
            setT();
            client.write(cmd);
        });
    }
}
function sendTxt(cell,msg,fn)
{
    gfn = fn;
    sendCommand("msg " + cell + " '" + msg + "'\n");
}
function sendPhoto(cell,photo,fn)
{
    gfn = fn;
    sendCommand("send_photo " + cell + " '" + photo + "'\n");
}
function addContact(cell,fn)
{
    gfn = fn;
    sendCommand("add_contact " + cell + " '" + cell + "' ''\n");
}
function updateDb(res,fn)
{
    //console.log(res);
    res = parseResult(res);
    if(res === 'FAIL')
    {
        addContact(datas[data_index-1].cell,function(res){
            if(res.split("\n").length>=2 && res.split("\n")[1]!='[]')
            {
                data_index --;
                fn();
            }
            else
            {
                res = "NOPHONE";
                var en = 5;

                connection.query('update outBox set en = '+en+' where id = '+datas[data_index-1].id,function(err, rows, fields){
                    if(typeof fn === 'function')
                    {
                        fn();
                    }
                });
                
            }
        });
    }
    else
    {
        var en = 2;
        if(res === 'SUCCESS')
        {
            en = 1;
        }
        else if(res === 'FAIL')
        {
            en = 4;
        }
        else if(res === 'TIMEOUT')
        {
            en = 3;
        }

        connection.query('update outBox set en = '+en+' where id = '+datas[data_index-1].id,function(err, rows, fields){
            if(typeof fn === 'function')
            {
                fn();
            }
        });
    }
}
function startSending()
{
    if(data_index<datas.length)
    {
        var cell = datas[data_index].cell;
        var msg  = datas[data_index].msg;
        var photo = datas[data_index].photo;
        data_index++;
        if(trim(photo)!=='')
        {
            sendPhoto(cell,photo,function(res1){
                if(trim(msg)!=='')
                {
                    sendTxt(cell,msg,function(res){
                        updateDb(res,function(){
                            startSending();
                        });
                    });
                }
                else
                {
                    updateDb(res1,function(){
                        startSending();
                    });
                }
            });
        }
        else if(trim(msg)!=='')
        {
            sendTxt(cell,msg,function(res){
                updateDb(res,function(){
                    startSending();
                });
            });
        }
        else
        {
            startSending();
        }
    }
    else
    {
        connection.end();
        process.exit();
    }
}