var net = require('net');
var ip = 'localhost';
var jsonfile = require('jsonfile')
        var conn = net.createConnection(4050, 'localhost');

        var completeData = '';

        conn.on('connect', function() {
                conn.write ("contact_list\n");
        });
        conn.on('data', function(data) {
                var read = data.toString();
                completeData += read;
		if(completeData.indexOf(']')>0)
		{
			var tmp = completeData.split("\n")[1];
			var obj = JSON.parse(tmp);
			jsonfile.writeFile('contact.json', obj, function (err) {
				console.error(err)
			});
			console.log(obj.length);
			conn.destroy();
		}
        });
        conn.on('end', function() {
                console.log("Connection closed to " + ip );
        });
        conn.on('error', function(err) {
                console.log("Connection error: " + err + " for ip " + ip);
        });
