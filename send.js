var net = require('net');
var http = require('http');
var server = "localhost";
var port = 4050;
var add_index = 9999;
var json_obj = [];
var client = new net.Socket();
var output = '';
var resp;
var resp1;
var sendCellFn;
var phone1;
var msg1;
var funcs = {};

function onData(cl, data) {
    output = '';
    if (String(data).split("\n").length > 1)
        output = String(data).split("\n")[1];
    if (resp)
    {
        resp.writeHead(200, {"Content-Type": "text/json"});
        resp.end(output);
    }
    else if (sendCellFn)
    {
        sendCellFn(output);
    }
//	cl.destroy();
}
client.on('data', function (data) {
    onData(client, data);
});

funcs.send = function (print_name, msg)
{
    client.connect(port, server, function () {
        client.write("msg " + print_name + " '" + msg + "'\n");
    });
};
funcs.sendCellPhoto = function (phone, msg)
{
    msg = "/root/tele/" + msg;
    sendCellFn = function (data) {
        var obj = JSON.parse(data);
        if (obj.result !== 'SUCCESS')
        {
            if (client.connectd)
            {
                client.write("add_contact " + phone1 + " '" + phone1 + "' ''\n");
            }
            else
            {
                client.destroy();
                client.connect(port, server, function () {
                    client.write("add_contact " + phone1 + " '" + phone1 + "' ''\n");
                });
            }
            sendCellFn = function (data) {
                sendCellFn = null;
                if (data !== '[]')
                {
                    if (client.connectd)
                    {
                        client.write("send_photo " + phone1 + " '" + msg1 + "'\n");
                    }
                    else
                    {
                        client.destroy();
                        resp = resp1;
                        sendCellFn = null;
                        client.connect(port, server, function () {
                            client.write("send_photo " + phone1 + " '" + msg1 + "'\n");
                        });
                    }

                }
                else
                {
                    data = '{"result": "NO_PHONE"}';
                    resp1.writeHead(200, {"Content-Type": "text/json"});
                    resp1.end(data);
                }
            };
        }
        else
        {
            resp1.writeHead(200, {"Content-Type": "text/json"});
            resp1.end(data);
        }
    };
    phone1 = phone;
    msg1 = msg;
    resp1 = resp;
    resp = null;
    client.connect(port, server, function () {
        client.write("send_photo " + phone + " '" + msg + "'\n");
    });
};
funcs.sendCell = function (phone, msg)
{
    sendCellFn = function (data) {
        var obj = JSON.parse(data);
        if (obj.result !== 'SUCCESS')
        {
            if (client.connectd)
            {
                client.write("add_contact " + phone1 + " '" + phone1 + "' ''\n");
            }
            else
            {
                client.destroy();
                client.connect(port, server, function () {
                    client.write("add_contact " + phone1 + " '" + phone1 + "' ''\n");
                });
            }
            sendCellFn = function (data) {
                sendCellFn = null;
                if (data !== '[]')
                {
                    if (client.connectd)
                    {
                        client.write("msg " + phone1 + " '" + msg1 + "'\n");
                    }
                    else
                    {
                        client.destroy();
                        resp = resp1;
                        sendCellFn = null;
                        client.connect(port, server, function () {
                            client.write("msg " + phone1 + " '" + msg1 + "'\n");
                        });
                    }

                }
                else
                {
                    data = '{"result": "NO_PHONE"}';
                    resp1.writeHead(200, {"Content-Type": "text/json"});
                    resp1.end(data);
                }
            };
            /*
             */
        }
        else
        {
            resp1.writeHead(200, {"Content-Type": "text/json"});
            resp1.end(data);
        }
    };
    phone1 = phone;
    msg1 = msg;
    resp1 = resp;
    resp = null;
    client.connect(port, server, function () {
        client.write("msg " + phone + " '" + msg + "'\n");
    });
};
var server = http.createServer(function (request, response) {
    if (request.url.split('?').length === 2)
    {
        var url = request.url.split('?')[1];
        var vars = url.split('&');
        var user = '';
        var msg = '';
        var fn = 'send';
        resp = response;
        for (var i = 0; i < vars.length; i++)
        {
            var key = vars[i].split('=')[0];
            var value = vars[i].split('=')[1];
            if (key === 'user')
                user = value;
            else if (key === 'msg')
                msg = decodeURI(value);
            else if (key === 'fn')
                fn = value;
        }
        funcs[fn](user, msg);
    }
});
server.listen(20);
console.log("Server running at http://127.0.0.1:20/");
